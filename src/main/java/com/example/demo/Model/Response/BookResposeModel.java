package com.example.demo.Model.Response;

public class BookResposeModel {
    private int id;
    private String title;
    private String autor;
    private String description;
    private String thumnail;
    private int category_id;

    public BookResposeModel() {
    }

    public BookResposeModel(int id, String title, String autor, String description, String thumnail, int category_id) {

        this.id = id;
        this.title = title;
        this.autor = autor;
        this.description = description;
        this.thumnail = thumnail;
        this.category_id = category_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumnail() {
        return thumnail;
    }

    public void setThumnail(String thumnail) {
        this.thumnail = thumnail;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "BookResposeModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", autor='" + autor + '\'' +
                ", description='" + description + '\'' +
                ", thumnail='" + thumnail + '\'' +
                ", category_id=" + category_id +
                '}';
    }
}
