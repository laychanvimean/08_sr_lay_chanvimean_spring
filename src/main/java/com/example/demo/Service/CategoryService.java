package com.example.demo.Service;

import com.example.demo.Model.DataTransfer.CategoryDto;

import java.util.List;

public interface CategoryService  {
    List<CategoryDto> findAll();
    CategoryDto insert (CategoryDto categoryDto);
    String delete (int d);
    CategoryDto update (int id, CategoryDto dto);
}
