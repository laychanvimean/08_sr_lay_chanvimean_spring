package com.example.demo.Service;


import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService{
    private CategoryRepository categoryRepository;


    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInsert=categoryRepository.insert(categoryDto);
        if(isInsert){
            return categoryDto;
        }else {
            return null;
        }
    }

    @Override
    public String delete(int id) {
        boolean isDeleted = categoryRepository.delete(id);
        if(isDeleted){
            return "YOU HAVE DELETE SCCESFULLY";
        }else {
            return "YOU CAN NOT DELETE";
        }
    }

    @Override
    public CategoryDto update(int id, CategoryDto dto) {
        boolean isUpdated = categoryRepository.update(id,dto);
        if(isUpdated){
            return dto;
        }else {
            return null;
        }
    }
}
