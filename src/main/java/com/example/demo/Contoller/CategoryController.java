package com.example.demo.Contoller;


import com.example.demo.Model.BaseApiResponse;
import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import com.example.demo.Model.Request.BookRequestModel;
import com.example.demo.Model.Request.CategoryRequestModel;
import com.example.demo.Model.Response.BookResposeModel;
import com.example.demo.Model.Response.CategoryResponseModel;
import com.example.demo.Service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryController {
    CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> findAll(){
        ModelMapper mapper = new ModelMapper();

        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();
        List<CategoryDto> bookDtoList = categoryService.findAll();
        List<CategoryRequestModel> category = new ArrayList<>();

        for (CategoryDto categoryDto : bookDtoList) {
            category.add(mapper.map(categoryDto,CategoryRequestModel.class));
        }
        response.setMessage("You have found all articles successfully");
        response.setData(category);
        response.setHttpStatus(HttpStatus.OK);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryResponseModel>> insert(@RequestBody CategoryRequestModel requestModel){
        System.out.println(requestModel);

        ModelMapper modelMapper = new ModelMapper();
        BaseApiResponse<CategoryResponseModel> respone=new BaseApiResponse <>();
        CategoryDto categoryDto = modelMapper.map(requestModel,CategoryDto.class);
        categoryService.insert(categoryDto);
        CategoryResponseModel responseModel = modelMapper.map(requestModel,CategoryResponseModel.class);

        respone.setMessage("YOU HAVE ADD SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.CREATED);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        respone.setData(responseModel);
        return new ResponseEntity <> (respone,HttpStatus.CREATED);
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        return new ResponseEntity <>(categoryService.delete(id),HttpStatus.OK);
    }
    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryResponseModel>> update(@PathVariable int id,@RequestBody CategoryRequestModel requestModel){
        ModelMapper modelMapper=new ModelMapper();
        BaseApiResponse<CategoryResponseModel> respone=new BaseApiResponse <>();

        CategoryDto dto=modelMapper.map(requestModel,CategoryDto.class);
        CategoryResponseModel responeModel=modelMapper.map(categoryService.update(id,dto),CategoryResponseModel.class);

        respone.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        respone.setHttpStatus(HttpStatus.OK);
        respone.setData(responeModel);
        respone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(respone);
    }




}
