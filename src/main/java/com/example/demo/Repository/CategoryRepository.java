package com.example.demo.Repository;


import com.example.demo.Model.DataTransfer.BookDto;
import com.example.demo.Model.DataTransfer.CategoryDto;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT * FROM tb_categories")
    List<CategoryDto> findAll();
    @Insert("INSERT INTO tb_categories (title) VALUES(#{title})")
    boolean insert(CategoryDto categoryDto);
    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete (int id);
    @Update("UPDATE "+ "\"tb_categories\" SET title=#{arg1.title} WHERE id=#{arg0}")
    boolean update(int id, CategoryDto categoryDto);


}
